<?php
class Student 
{

function avg()
{

    echo ( ( 80 + 70 ) / 2 )."\n";
}
}
$a001 = new Student();
$a001->avg();

echo "Class có tham số"."\n";

class Student1
{

function avg($math, $english)
{

    echo ( ( $math + $english ) / 2 )."\n";
}
}
$a002 = new Student1();
$a002->avg(30, 70)."\n\n\n";


echo "PROPERTY"."\n";
class Student2
{
public $name;
function avg($math, $english)
{

    echo ( ( $math + $english ) / 2 )."\n";
}
}
$a002 = new Student1();
$a002->avg(80, 70)."\n\n\n";
$a002->name="sato";
echo $a002->name."\n\n\n";


echo "HÀM TẠO - CONSTRUCTOR"."\n";
class Student3
{
public $name;
function __construct($name) {
    $this->name = $name;
  }

function avg($math, $english)
{

    echo ( ( $math + $english ) / 2 )."\n";
}
}
// $a003 = new Student3('jhj',9);
// $a003->name="sato";
//echo $a003->name."\n";
$a004 = new Student3('tanaka');
echo $a004->name."\n";